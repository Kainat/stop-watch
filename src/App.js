import React from 'react';
import './App.css';
import { StopWatch,StopWatchV1 } from './components';

function App() {
  return (
    <div className="App" >
     <StopWatch/>
    </div>
  );
}

export default App;
