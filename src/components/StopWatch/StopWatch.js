import React, { useState, useEffect } from "react";
import "./Style.scss";
import { stopWatchState } from "../meta";


export default function StopWatch() {
  const [state, setState] = useState(
    stopWatchState
  );

  function toggle() {
    setState((prev) => ({
      ...prev,
      isActive: !state.isActive,
    }));
  }

  function reset() {
    setState((prev) => ({
      ...prev,
      seconds: 0,
    }));
  }

  const caclSecs = (seconds) => {
    if (seconds > 59) return seconds % 60;
    return seconds;
  };

  const calcMints = (seconds) => {
    if (seconds > 59) {
      const mints = (seconds - calcHours(seconds) * 3600) / 60;
      return Math.floor(mints);
    }
    return 0;
  };

  const calcHours = (seconds) => {
    if (seconds > 59) {
      const hours = seconds / 3600;
      return Math.floor(hours);
    }
    return 0;
  };

  const calcSplitSeconds = () => {
    const currSplitValue = state.seconds - state.splitSecs;
    const splitSeconds = caclSecs(currSplitValue);
    const splitMinutes = calcMints(currSplitValue);
    const splitHours = calcHours(currSplitValue);
    setState((prev) => ({
      ...prev,
      splitSecs: 0,
    }));
    addItem("Split", splitHours, splitMinutes, splitSeconds);
  };

  useEffect(() => {
    let interval = null;
    if (calcHours(state.seconds) > 24) {
      setState((prev) => ({
        ...prev,
        seconds: 0,
      }));
    }
    if (state.isActive) {
      interval = setInterval(() => {
        setState((prev) => ({
          ...prev,
          seconds: state.seconds + 1,
        }));
      }, 1000);
    } else if (!state.isActive && state.seconds !== 0) {
      clearInterval(interval);
      addItem(
        "Pause",
        calcHours(state.seconds),
        calcMints(state.seconds),
        caclSecs(state.seconds)
      );
    }

    return () => {
      clearInterval(interval);
    };
  }, [state.splitSecs, state.seconds, state.isActive]);

  const addItem = (event, hours, minutes, seconds) => {
    setState((prev) => ({
      ...prev,
      items: [
        ...state.items,
        {
          id: state.items.length,
          event: event,
          value: `${hours} H : ${minutes} M : ${seconds} S`,
        },
      ],
    }));
  };

  return (
    <main className="stopwatch-wrapper">
      <section className="phone-wrapper">
        <h1>Stop Watch</h1>

        <section className="watch">
          <article className="dotted-circle">
            {calcHours(state.seconds)} : {calcMints(state.seconds)} :{" "}
            {caclSecs(state.seconds)}
          </article>
        </section>

        <button className="btn" onClick={toggle}>
          {state.isActive ? "Pause" : "Start"}
        </button>

        <button
          className={!state.isActive ? "disable" : "btn"}
          onClick={!state.isActive ? null : calcSplitSeconds}
        >
          Split
        </button>

        <button
          className={!state.isActive ? "reset" : "disable"}
          onClick={!state.isActive ? reset : null}
        >
          Reset
        </button>

        <ul className="events-log">
          {state.items.map((item) => (
            <li key={item.id}>
              {item.event} <div> {item.value}</div>
            </li>
          ))}
        </ul>
      </section>
    </main>
  );
}
