import React, { useState, useEffect } from "react";
import "./Style.scss";

export default function StopWatchV1() {
  const [seconds, setSeconds] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [hours, sethours] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [items, setItems] = useState([]);
  const [splitSecond, setSplitSecond] = useState(0);
  const [splitMinutes, setSplitMinutes] = useState(0);
  const [splitHours, setSplitHours] = useState(0);
  const [splitActive, setSplitActive] = useState(false);

  function toggle() {
    setIsActive(!isActive);
  }

  function reset() {
    setSeconds(0);
    setIsActive(false);
  }

  function splitTime() {
    setSplitActive(!splitActive);
  }

  useEffect(() => {
    let interval = null;
    if (isActive) {
      interval = setInterval(() => {
        setSplitSecond(splitSecond + 1);

        if (splitMinutes == 59) {
          setSplitHours((splitHours) => splitHours + 1);
          setSplitMinutes(0);
          setSplitSecond(0);
        }

        if (splitSecond == 59) {
          setSplitMinutes((splitMinutes) => splitMinutes + 1);
          setSplitSecond(0);
        }

        if (splitActive) {
          addItem("Split", splitHours, splitMinutes, splitSecond);

          setSplitSecond(0);
          setSplitMinutes(0);
          setSplitHours(0);

          setSplitActive(!splitActive);
        }

        if (minutes == 59) {
          setSeconds(0);
          setMinutes(0);
          sethours((hours) => hours + 1);
        } else {
          if (seconds === 59) {
            setSeconds(0);
            setMinutes((minutes) => minutes + 1);
          } else {
            setSeconds(seconds + 1);
          }
        }
      }, 1000);
    } else if (!isActive && seconds !== 0) {
      clearInterval(interval);
      addItem("Pause", hours, minutes, seconds);
    }
    return () => clearInterval(interval);
  }, [seconds, minutes, isActive, splitActive]);

  const addItem = (event, hours, minutes, seconds) => {
    setItems((items) => [
      ...items,
      {
        id: items.length,
        event: event,
        value: `${hours} H : ${minutes} M : ${seconds} S`,
      },
    ]);
  };

  return (
    <main className="stopwatch-wrapper">
      <section className="phone-wrapper">
        <h1>Stop Watch</h1>

        <section className="watch">
          <article className="dotted-circle">
            {hours} : {minutes} : {seconds}
          </article>
        </section>

        <button className="btn" onClick={toggle}>
          {isActive ? "Pause" : "Start"}
        </button>

        <button
          className={!isActive ? "disable" : "btn"}
          onClick={!isActive ? null : splitTime}
        >
          Split
        </button>

        <button
          className={!isActive ? "reset" : "disable"}
          onClick={!isActive ? reset : null}
        >
          Reset
        </button>

        <ul className="events-log">
          {items.map((item) => (
            <li key={item.id}>
              {item.event} <div> {item.value}</div>
            </li>
          ))}
        </ul>
      </section>
    </main>
  );
}
